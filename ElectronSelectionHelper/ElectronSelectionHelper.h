#include "xAODEgamma/Electron.h"

class ElectronSelectionHelper {

 public:

  // constructor
  ElectronSelectionHelper() {};

  // destructor
  virtual ~ElectronSelectionHelper() {};

  // checks the kinematics of the object
  bool isElectronGood(const xAOD::Electron* muon);

};
